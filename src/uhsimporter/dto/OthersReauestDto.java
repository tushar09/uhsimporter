package uhsimporter.dto;

/**
 * Created by Tushar on 8/29/2017.
 */

public class OthersReauestDto{

    private double lat, lon;
    private String name, email, phone;
    private long type;

    public double getLat(){
        return lat;
    }

    public void setLat(double lat){
        this.lat = lat;
    }

    public double getLon(){
        return lon;
    }

    public void setLon(double lon){
        this.lon = lon;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getPhone(){
        return phone;
    }

    public void setPhone(String phone){
        this.phone = phone;
    }

    public long getType(){
        return type;
    }

    public void setType(long type){
        this.type = type;
    }

    @Override
    public String toString(){
        return "OthersReauestDto{" +
                "lat=" + lat +
                ", lon=" + lon +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", type=" + type +
                '}';
    }
}
