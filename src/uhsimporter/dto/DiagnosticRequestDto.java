package uhsimporter.dto;



/**
 * Created by Tushar on 8/30/2017.
 */

public class DiagnosticRequestDto{
    private double lat, lon;
    private int tesla,
            ctScan,
            ctScanSixFour,
            xray,
            ecg,
            holter,
            bp,
            mammo,
            ultraSound,
            pulmonary,
            ercp,
            clinicalLaboratory,
            home;

    private String name, email, phone;
    private int type;

    public double getLat(){
        return lat;
    }

    public void setLat(double lat){
        this.lat = lat;
    }

    public double getLon(){
        return lon;
    }

    public void setLon(double lon){
        this.lon = lon;
    }

    public int getTesla(){
        return tesla;
    }

    public void setTesla(int tesla){
        this.tesla = tesla;
    }

    public int getCtScan(){
        return ctScan;
    }

    public void setCtScan(int ctScan){
        this.ctScan = ctScan;
    }

    public int getCtScanSixFour(){
        return ctScanSixFour;
    }

    public void setCtScanSixFour(int isCtScanSixFour){
        this.ctScanSixFour = isCtScanSixFour;
    }

    public int getXray(){
        return xray;
    }

    public void setXray(int xray){
        this.xray = xray;
    }

    public int getEcg(){
        return ecg;
    }

    public void setEcg(int ecg){
        this.ecg = ecg;
    }

    public int getHolter(){
        return holter;
    }

    public void setHolter(int holter){
        this.holter = holter;
    }

    public int getBp(){
        return bp;
    }

    public void setBp(int bp){
        this.bp = bp;
    }

    public int getMammo(){
        return mammo;
    }

    public void setMammo(int mammo){
        this.mammo = mammo;
    }

    public int getUltraSound(){
        return ultraSound;
    }

    public void setUltraSound(int ultraSound){
        this.ultraSound = ultraSound;
    }

    public int getPulmonary(){
        return pulmonary;
    }

    public void setPulmonary(int pulmonary){
        this.pulmonary = pulmonary;
    }

    public int getErcp(){
        return ercp;
    }

    public void setErcp(int ercp){
        this.ercp = ercp;
    }

    public int getClinicalLaboratory(){
        return clinicalLaboratory;
    }

    public void setClinicalLaboratory(int clinicalLaboratory){
        this.clinicalLaboratory = clinicalLaboratory;
    }

    public int getHome(){
        return home;
    }

    public void setHome(int home){
        this.home = home;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getPhone(){
        return phone;
    }

    public void setPhone(String phone){
        this.phone = phone;
    }

    public int getType(){
        return type;
    }

    public void setType(int type){
        this.type = type;
    }

    @Override
    public String toString(){
        return "DiagnosticRequestDto{" +
                "lat=" + lat +
                ", lon=" + lon +
                ", tesla=" + tesla +
                ", ctScan=" + ctScan +
                ", ctScanSixFour=" + ctScanSixFour +
                ", xray=" + xray +
                ", ecg=" + ecg +
                ", holter=" + holter +
                ", bp=" + bp +
                ", mammo=" + mammo +
                ", ultraSound=" + ultraSound +
                ", pulmonary=" + pulmonary +
                ", ercp=" + ercp +
                ", clinicalLaboratory=" + clinicalLaboratory +
                ", home=" + home +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", type=" + type +
                '}';
    }
}
