package uhsimporter.dto;

/**
 * Created by Tushar on 8/28/2017.
 */

public class HospitalRequestDto{

    private double lat, lon;
    private int emergency,
                icu,
                childCare,
                dental,
                cardilogy,
                surgery,
                medicine,
                maternityCare,
                skinCare,
                ambulance;
    private String name, email, phone;
    private int type;

    //------lat lng------
    public double getLat(){
        return lat;
    }

    public void setLat(double lat){
        this.lat = lat;
    }

    public double getLon(){
        return lon;
    }

    public void setLon(double lon){
        this.lon = lon;
    }


    //------hospital facilities------
    public int getEmergency(){
        return emergency;
    }

    public void setEmergency(int emergency){
        this.emergency = emergency;
    }

    public int getIcu(){
        return icu;
    }

    public void setIcu(int icu){
        this.icu = icu;
    }

    public int getChildCare(){
        return childCare;
    }

    public void setChildCare(int childCare){
        this.childCare = childCare;
    }

    public int getDental(){
        return dental;
    }

    public void setDental(int dental){
        this.dental = dental;
    }

    public int getCardilogy(){
        return cardilogy;
    }

    public void setCardilogy(int cardilogy){
        this.cardilogy = cardilogy;
    }

    public int getSurgery(){
        return surgery;
    }

    public void setSurgery(int surgery){
        this.surgery = surgery;
    }

    public int getMedicine(){
        return medicine;
    }

    public void setMedicine(int medicine){
        this.medicine = medicine;
    }

    public int getMaternityCare(){
        return maternityCare;
    }

    public void setMaternityCare(int maternityCare){
        this.maternityCare = maternityCare;
    }

    public int getSkinCare(){
        return skinCare;
    }

    public void setSkinCare(int skinCare){
        this.skinCare = skinCare;
    }

    public int getAmbulance(){
        return ambulance;
    }

    public void setAmbulance(int ambulance){
        this.ambulance = ambulance;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getPhone(){
        return phone;
    }

    public void setPhone(String phone){
        this.phone = phone;
    }

    public int getType(){
        return type;
    }

    public void setType(int type){
        this.type = type;
    }

    @Override
    public String toString(){
        return "HospitalRequestDto{" +
                "lat=" + lat +
                ", lon=" + lon +
                ", emergency=" + emergency +
                ", icu=" + icu +
                ", childCare=" + childCare +
                ", dental=" + dental +
                ", cardilogy=" + cardilogy +
                ", surgery=" + surgery +
                ", medicine=" + medicine +
                ", maternityCare=" + maternityCare +
                ", skinCare=" + skinCare +
                ", ambulance=" + ambulance +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", type=" + type +
                '}';
    }
}
